/// Represents an RGBA color.
#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Color {
    pub const WHITE: Self = Self {
        r: 0xFF,
        g: 0xFF,
        b: 0xFF,
        a: 0xFF,
    };
    pub const BLACK: Self = Self {
        r: 0x00,
        g: 0x00,
        b: 0x00,
        a: 0xFF,
    };
    pub const TRANSPARENT: Self = Self {
        r: 0x00,
        g: 0x00,
        b: 0x00,
        a: 0x00,
    };

    /// Converts this color into shader data (normalized RGBA).
    pub(crate) fn to_shader_data(&self) -> [f32; 4] {
        [
            self.r as f32 / 255.0,
            self.g as f32 / 255.0,
            self.b as f32 / 255.0,
            self.a as f32 / 255.0,
        ]
    }
}

/// Allows constructing a color from RGBA bytes.
impl From<(u8, u8, u8, u8)> for Color {
    fn from((r, g, b, a): (u8, u8, u8, u8)) -> Self {
        Self { r, g, b, a }
    }
}

/// Allows constructing an opaque color from RGB bytes.
impl From<(u8, u8, u8)> for Color {
    fn from((r, g, b): (u8, u8, u8)) -> Self {
        Self { r, g, b, a: 0xFF }
    }
}
