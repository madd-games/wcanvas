use std::slice;

use crate::{
    color::Color,
    geo::{Dims, Pos},
};

mod from_image;

pub use self::from_image::*;

/// Represents a bitmap of colors.
#[derive(Clone)]
pub struct Bitmap {
    size: Dims,
    colors: Vec<Color>,
}

impl Bitmap {
    /// Construct a bitmap of the specified size, filled with transparency.
    pub fn new(size: &Dims) -> Self {
        Self {
            size: *size,
            colors: vec![Color::TRANSPARENT; (size.width * size.height) as usize],
        }
    }

    /// Get the size of this bitmap.
    pub fn size(&self) -> &Dims {
        &self.size
    }

    /// Return an immutable reference to the color at the specified position.
    ///
    /// Returns `None` if the position is out of bounds.
    pub fn at(&self, pos: Pos) -> Option<&Color> {
        if pos.x < 0
            || pos.x >= self.size.width as i32
            || pos.y < 0
            || pos.y >= self.size.height as i32
        {
            None
        } else {
            Some(&self.colors[pos.y as usize * self.size.width as usize + pos.x as usize])
        }
    }

    /// Return a mutable reference to the color at the specified position.
    ///
    /// Returns `None` if the position is out of bounds.
    pub fn at_mut(&mut self, pos: Pos) -> Option<&mut Color> {
        if pos.x < 0
            || pos.x >= self.size.width as i32
            || pos.y < 0
            || pos.y >= self.size.height as i32
        {
            None
        } else {
            Some(&mut self.colors[pos.y as usize * self.size.width as usize + pos.x as usize])
        }
    }

    /// Returns the slice of colors.
    pub fn colors(&self) -> &[Color] {
        &self.colors
    }

    /// Returns the mutable slice of colors.
    pub fn colors_mut(&mut self) -> &mut [Color] {
        &mut self.colors
    }

    /// Returns the slice of RGBA bytes.
    pub fn rgba_bytes(&self) -> &[u8] {
        unsafe {
            slice::from_raw_parts(
                self.colors() as *const _ as *const u8,
                4 * self.colors.len(),
            )
        }
    }

    /// Returns the mutable slice of RGBA bytes.
    pub fn rgba_bytes_mut(&mut self) -> &mut [u8] {
        unsafe {
            slice::from_raw_parts_mut(
                self.colors_mut() as *mut _ as *mut u8,
                4 * self.colors.len(),
            )
        }
    }
}
