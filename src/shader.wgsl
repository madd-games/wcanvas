// Vertex shader

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) color: vec4<f32>,
    @location(1) tex_coords: vec2<f32>,
}

struct ShaderData {
    pos: vec2<f32>,
    size: vec2<f32>,
    tex_pos: vec2<f32>,
    tex_size: vec2<f32>,
    color0: vec4<f32>,
    color1: vec4<f32>,
    color2: vec4<f32>,
    color3: vec4<f32>,
}

var<push_constant> shader_data : ShaderData;

@group(0) @binding(0)
var tex: texture_2d<f32>;

@group(0) @binding(1)
var tex_sampler: sampler;

@vertex
fn vs_main(
    @builtin(vertex_index) in_vertex_index: u32,
) -> VertexOutput {
    var out: VertexOutput;
    let coord_x = select(0.0, 1.0, in_vertex_index >= 2u && in_vertex_index < 5u);
    let coord_y = select(1.0, 0.0, in_vertex_index >= 1u && in_vertex_index < 4u);
    out.clip_position = vec4(vec2(coord_x, coord_y) * shader_data.size + shader_data.pos, 0.0, 1.0);
    out.tex_coords = vec2(coord_x, 1.0 - coord_y) * shader_data.tex_size + shader_data.tex_pos;

    if coord_x == 0.0 && coord_y == 1.0 {
        out.color = shader_data.color0;
    }
    else if coord_x == 1.0 && coord_y == 1.0 {
        out.color = shader_data.color1;
    }
    else if coord_x == 1.0 && coord_y == 0.0 {
        out.color = shader_data.color2;
    }
    else {
        out.color = shader_data.color3;
    }

    return out;
}

// Fragment shader

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    return textureSample(tex, tex_sampler, in.tex_coords) * in.color;
}

