use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Sub, SubAssign};

/// Represents the dimensions of an object.
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Dims {
    pub width: u32,
    pub height: u32,
}

impl Dims {
    /// Construct a pair of dimensions.
    pub fn new(width: u32, height: u32) -> Self {
        Self { width, height }
    }
}

impl Add<Dims> for Dims {
    type Output = Dims;

    fn add(self, rhs: Dims) -> Self::Output {
        Self::new(self.width + rhs.width, self.height + rhs.height)
    }
}

impl AddAssign<Dims> for Dims {
    fn add_assign(&mut self, rhs: Dims) {
        self.width += rhs.width;
        self.height += rhs.height;
    }
}

impl Sub<Dims> for Dims {
    type Output = Dims;

    fn sub(self, rhs: Dims) -> Self::Output {
        Self::new(self.width - rhs.width, self.height - rhs.height)
    }
}

impl SubAssign<Dims> for Dims {
    fn sub_assign(&mut self, rhs: Dims) {
        self.width -= rhs.width;
        self.height -= rhs.height;
    }
}

impl Mul<u32> for Dims {
    type Output = Self;

    fn mul(self, rhs: u32) -> Self::Output {
        Self::new(self.width * rhs, self.height * rhs)
    }
}

impl MulAssign<u32> for Dims {
    fn mul_assign(&mut self, rhs: u32) {
        self.width *= rhs;
        self.height *= rhs;
    }
}

impl Mul<Dims> for Dims {
    type Output = Self;

    fn mul(self, rhs: Dims) -> Self::Output {
        Self::new(self.width * rhs.width, self.height * rhs.height)
    }
}

impl MulAssign<Dims> for Dims {
    fn mul_assign(&mut self, rhs: Dims) {
        self.width *= rhs.width;
        self.height *= rhs.height;
    }
}

impl Div<u32> for Dims {
    type Output = Self;

    fn div(self, rhs: u32) -> Self::Output {
        Self::new(self.width / rhs, self.height / rhs)
    }
}

impl DivAssign<u32> for Dims {
    fn div_assign(&mut self, rhs: u32) {
        self.width /= rhs;
        self.height /= rhs;
    }
}

impl Div<Dims> for Dims {
    type Output = Self;

    fn div(self, rhs: Dims) -> Self::Output {
        Self::new(self.width / rhs.width, self.height / rhs.height)
    }
}

impl DivAssign<Dims> for Dims {
    fn div_assign(&mut self, rhs: Dims) {
        self.width /= rhs.width;
        self.height /= rhs.height;
    }
}
