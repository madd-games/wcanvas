use super::{Dims, Pos};

/// Represents a rectangle.
#[derive(Debug, Clone, Copy)]
pub struct Rect {
    pub pos: Pos,
    pub size: Dims,
}

/// Allows constructing a `Rect` from a `(pos, size)` tuple.
impl From<(Pos, Dims)> for Rect {
    fn from((pos, size): (Pos, Dims)) -> Self {
        Self { pos, size }
    }
}

/// Allows constructing a `Rect` from a `(x, y, width, height)` tuple.
impl From<(i32, i32, u32, u32)> for Rect {
    fn from((x, y, width, height): (i32, i32, u32, u32)) -> Self {
        Self {
            pos: Pos { x, y },
            size: Dims { width, height },
        }
    }
}
