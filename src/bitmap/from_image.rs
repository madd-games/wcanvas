use image::{DynamicImage, EncodableLayout};

use crate::geo::Dims;

use super::Bitmap;

impl From<DynamicImage> for Bitmap {
    fn from(image: DynamicImage) -> Self {
        let rgba = image.to_rgba8();

        let mut bitmap = Self::new(&Dims::new(image.width(), image.height()));
        bitmap.rgba_bytes_mut().copy_from_slice(rgba.as_bytes());

        bitmap
    }
}
