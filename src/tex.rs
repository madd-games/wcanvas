use crate::geo::{Dims, Pos, Rect};

/// Represents a texture useable with drawing calls.
pub struct Texture {
    pub(crate) bind_group_index: u64,
    pub(crate) size: Dims,
}

impl Texture {
    /// Get the size of this texture.
    pub fn size(&self) -> &Dims {
        &self.size
    }

    /// Return a rect covering the whole texture.
    pub fn whole_rect(&self) -> Rect {
        (Pos::new(0, 0), self.size).into()
    }
}
