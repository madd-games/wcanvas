use std::{collections::BTreeMap, error::Error, mem, slice};

use winit::{dpi::PhysicalSize, window::Window};

use crate::{
    bitmap::Bitmap,
    brush::Brush,
    color::Color,
    ctx::GraphicsContext,
    error::WgpuBackendError,
    geo::{Dims, Pos},
    tex::Texture,
};

/// Trait implemented by types which can be used as canvas backends.
#[async_trait::async_trait]
pub trait CanvasBackend {
    type CanvasType;

    /// Create a canvas for this backend, and take ownership of it.
    async fn create_canvas(self) -> Result<Self::CanvasType, Box<dyn Error>>;
}

/// The data passed to the shader as push constants.
#[derive(Default)]
#[repr(C)]
pub(crate) struct ShaderData {
    pub pos: [f32; 2],
    pub size: [f32; 2],
    pub tex_pos: [f32; 2],
    pub tex_size: [f32; 2],
    pub brush: [[f32; 4]; 4],
}

impl ShaderData {
    /// Return the shader data as a byte slice.
    pub fn as_bytes(&self) -> &[u8] {
        unsafe {
            slice::from_raw_parts(self as *const _ as *const u8, mem::size_of::<ShaderData>())
        }
    }
}

/// Represents a canvas, which wraps a backend and allows drawing to it.
pub struct Canvas<B: CanvasBackend> {
    backend: B,
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    config: wgpu::SurfaceConfiguration,
    size: PhysicalSize<u32>,
    render_pipeline: wgpu::RenderPipeline,
    texture_bind_group_layout: wgpu::BindGroupLayout,
    bind_group_table: BTreeMap<u64, wgpu::BindGroup>,
    next_bind_group_index: u64,
    white_texture: Texture,
}

/// Allows graphics contexts to be created for windows.
#[async_trait::async_trait]
impl CanvasBackend for Window {
    type CanvasType = Canvas<Window>;

    async fn create_canvas(self) -> Result<Canvas<Self>, Box<dyn Error>> {
        let instance = wgpu::Instance::new(wgpu::InstanceDescriptor {
            backends: wgpu::Backends::all(),
            dx12_shader_compiler: Default::default(),
        });

        let surface = unsafe { instance.create_surface(&self) }.unwrap();

        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::default(),
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .ok_or(WgpuBackendError::AdapterNotFound)?;

        let mut limits = wgpu::Limits::default();
        limits.max_push_constant_size = mem::size_of::<ShaderData>() as u32;

        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::PUSH_CONSTANTS,
                    limits,
                    label: None,
                },
                None, // Trace path
            )
            .await?;

        let size = self.inner_size();
        let surface_caps = surface.get_capabilities(&adapter);
        // Shader code in this tutorial assumes an sRGB surface texture. Using a different
        // one will result all the colors coming out darker. If you want to support non
        // sRGB surfaces, you'll need to account for that when drawing to the frame.
        let surface_format = surface_caps
            .formats
            .iter()
            .copied()
            .find(|f| f.is_srgb())
            .unwrap_or(surface_caps.formats[0]);
        let config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface_format,
            width: size.width,
            height: size.height,
            present_mode: surface_caps.present_modes[0],
            alpha_mode: surface_caps.alpha_modes[0],
            view_formats: vec![],
        };
        surface.configure(&device, &config);

        let texture_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            multisampled: false,
                            view_dimension: wgpu::TextureViewDimension::D2,
                            sample_type: wgpu::TextureSampleType::Float { filterable: true },
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        // This should match the filterable field of the
                        // corresponding Texture entry above.
                        ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                        count: None,
                    },
                ],
                label: Some("texture_bind_group_layout"),
            });

        let shader = device.create_shader_module(wgpu::include_wgsl!("shader.wgsl"));
        let render_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Render Pipeline Layout"),
                bind_group_layouts: &[&texture_bind_group_layout],
                push_constant_ranges: &[wgpu::PushConstantRange {
                    range: 0u32..mem::size_of::<ShaderData>() as u32,
                    stages: wgpu::ShaderStages::VERTEX,
                }],
            });

        let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("Render Pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[],
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(wgpu::ColorTargetState {
                    format: config.format,
                    blend: Some(wgpu::BlendState::ALPHA_BLENDING),
                    write_mask: wgpu::ColorWrites::ALL,
                })],
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: Some(wgpu::Face::Back),
                // Setting this to anything other than Fill requires Features::NON_FILL_POLYGON_MODE
                polygon_mode: wgpu::PolygonMode::Fill,
                // Requires Features::DEPTH_CLIP_CONTROL
                unclipped_depth: false,
                // Requires Features::CONSERVATIVE_RASTERIZATION
                conservative: false,
            },
            depth_stencil: None,
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        });

        let mut bitmap = Bitmap::new(&Dims::new(1, 1));
        *bitmap.at_mut(Pos::new(0, 0)).unwrap() = Color::WHITE;

        let mut canvas = Canvas {
            backend: self,
            surface,
            device,
            queue,
            config,
            size,
            render_pipeline,
            texture_bind_group_layout,
            bind_group_table: BTreeMap::new(),
            next_bind_group_index: 0,
            white_texture: Texture {
                bind_group_index: 0,
                size: Dims::new(1, 1),
            },
        };

        // This will create the white texture with bind group index 0.
        canvas.create_texture(&bitmap).unwrap();

        Ok(canvas)
    }
}

impl Canvas<Window> {
    /// Return the wrapped window.
    pub fn window(&self) -> &Window {
        &self.backend
    }

    /// Draw a frame.
    ///
    /// The callback `f` will be called, and passed the graphics context for this frame.
    pub fn draw_frame(
        &mut self,
        f: impl FnOnce(&mut GraphicsContext),
    ) -> Result<(), Box<dyn Error>> {
        let current_size = self.backend.inner_size();
        if current_size != self.size {
            self.size = current_size;
            self.config.width = self.size.width;
            self.config.height = self.size.height;
            self.surface.configure(&self.device, &self.config);
        }

        let output = self.surface.get_current_texture()?;
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Render Pass"),
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                        store: true,
                    },
                })],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&self.render_pipeline);

            let mut context = GraphicsContext {
                render_pass,
                shader_data: ShaderData::default(),
                viewport: (0, 0, current_size.width, current_size.height).into(),
                canvas_size: Dims::new(current_size.width, current_size.height),
                bind_group_table: &self.bind_group_table,
            };

            context.set_texture(&self.white_texture, &self.white_texture.whole_rect());
            context.set_brush(Brush::white());

            (f)(&mut context);
        }

        // submit will accept anything that implements IntoIter
        let cmdbuf = encoder.finish();
        self.queue.submit(std::iter::once(cmdbuf));
        output.present();

        Ok(())
    }
}

impl<B: CanvasBackend> Canvas<B> {
    /// Tear down the canvas and return the wrapped backend.
    pub fn tear_down(self) -> B {
        self.backend
    }

    /// Create a texture from the specified bitmap.
    pub fn create_texture(&mut self, bitmap: &Bitmap) -> Result<Texture, Box<dyn Error>> {
        let texture_size = wgpu::Extent3d {
            width: bitmap.size().width,
            height: bitmap.size().height,
            depth_or_array_layers: 1,
        };
        let texture = self.device.create_texture(&wgpu::TextureDescriptor {
            // All textures are stored as 3D, we represent our 2D texture
            // by setting depth to 1.
            size: texture_size,
            mip_level_count: 1, // We'll talk about this a little later
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            // Most images are stored using sRGB so we need to reflect that here.
            format: wgpu::TextureFormat::Rgba8UnormSrgb,
            // TEXTURE_BINDING tells wgpu that we want to use this texture in shaders
            // COPY_DST means that we want to copy data to this texture
            usage: wgpu::TextureUsages::TEXTURE_BINDING | wgpu::TextureUsages::COPY_DST,
            label: None,
            // This is the same as with the SurfaceConfig. It
            // specifies what texture formats can be used to
            // create TextureViews for this texture. The base
            // texture format (Rgba8UnormSrgb in this case) is
            // always supported. Note that using a different
            // texture format is not supported on the WebGL2
            // backend.
            view_formats: &[],
        });

        self.queue.write_texture(
            // Tells wgpu where to copy the pixel data
            wgpu::ImageCopyTexture {
                texture: &texture,
                mip_level: 0,
                origin: wgpu::Origin3d::ZERO,
                aspect: wgpu::TextureAspect::All,
            },
            // The actual pixel data
            bitmap.rgba_bytes(),
            // The layout of the texture
            wgpu::ImageDataLayout {
                offset: 0,
                bytes_per_row: Some(4 * bitmap.size().width),
                rows_per_image: Some(bitmap.size().height),
            },
            texture_size,
        );

        // We don't need to configure the texture view much, so let's
        // let wgpu define it.
        let texture_view = texture.create_view(&wgpu::TextureViewDescriptor::default());
        let sampler = self.device.create_sampler(&wgpu::SamplerDescriptor {
            address_mode_u: wgpu::AddressMode::Repeat,
            address_mode_v: wgpu::AddressMode::Repeat,
            address_mode_w: wgpu::AddressMode::Repeat,
            mag_filter: wgpu::FilterMode::Linear,
            min_filter: wgpu::FilterMode::Nearest,
            mipmap_filter: wgpu::FilterMode::Nearest,
            ..Default::default()
        });

        let bind_group = self.device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &self.texture_bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: wgpu::BindingResource::TextureView(&texture_view),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: wgpu::BindingResource::Sampler(&sampler),
                },
            ],
            label: None,
        });

        let bind_group_index = self.next_bind_group_index;
        self.next_bind_group_index += 1;
        self.bind_group_table.insert(bind_group_index, bind_group);

        Ok(Texture {
            bind_group_index,
            size: *bitmap.size(),
        })
    }
}
