//! Module defining the manipulation of graphics contexts.

use std::collections::BTreeMap;

use crate::{
    brush::Brush,
    canvas::ShaderData,
    geo::{Dims, Rect},
    tex::Texture,
};

/// Represents a graphics context.
pub struct GraphicsContext<'a> {
    pub(crate) render_pass: wgpu::RenderPass<'a>,
    pub(crate) shader_data: ShaderData,
    pub(crate) viewport: Rect,
    pub(crate) canvas_size: Dims,
    pub(crate) bind_group_table: &'a BTreeMap<u64, wgpu::BindGroup>,
}

impl<'a> GraphicsContext<'a> {
    /// Returns the size of the canvas.
    pub fn canvas_size(&self) -> &Dims {
        &self.canvas_size
    }

    /// Returns the size of the current view.
    pub fn view_size(&self) -> &Dims {
        &self.viewport.size
    }

    /// Set the brush to use for future draw calls.
    pub fn set_brush(&mut self, brush: &Brush) {
        self.shader_data.brush = brush.to_shader_data();
    }

    /// Set the texture to use for future draw calls.
    pub fn set_texture(&mut self, texture: &Texture, rect: &Rect) {
        self.shader_data.tex_pos[0] = rect.pos.x as f32 / texture.size.width as f32;
        self.shader_data.tex_pos[1] = rect.pos.y as f32 / texture.size.height as f32;

        self.shader_data.tex_size[0] = rect.size.width as f32 / texture.size.width as f32;
        self.shader_data.tex_size[1] = rect.size.height as f32 / texture.size.height as f32;

        self.render_pass.set_bind_group(
            0,
            self.bind_group_table
                .get(&texture.bind_group_index)
                .unwrap(),
            &[],
        );
    }

    /// Fill in a rectangle with the current brush.
    pub fn fill_rect(&mut self, rect: &Rect) {
        let fwidth = self.viewport.size.width as f32;
        let fheight = self.viewport.size.height as f32;

        let converted_y = self.viewport.size.height as i32 - rect.pos.y - rect.size.height as i32;

        self.shader_data.pos[0] = (2.0 * rect.pos.x as f32 / fwidth) - 1.0;
        self.shader_data.pos[1] = (2.0 * converted_y as f32 / fheight) - 1.0;

        self.shader_data.size[0] = 2.0 * rect.size.width as f32 / fwidth;
        self.shader_data.size[1] = 2.0 * rect.size.height as f32 / fheight;

        self.render_pass.set_push_constants(
            wgpu::ShaderStages::VERTEX,
            0,
            self.shader_data.as_bytes(),
        );
        self.render_pass.draw(0..6, 0..1);
    }
}
