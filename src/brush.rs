use crate::color::Color;

/// A combination of colors for each corner of a rectangle.
#[derive(Debug, Clone)]
pub struct Brush {
    pub colors: [Color; 4],
}

static WHITE_BRUSH: Brush = Brush {
    colors: [Color::WHITE, Color::WHITE, Color::WHITE, Color::WHITE],
};

static BLACK_BRUSH: Brush = Brush {
    colors: [Color::BLACK, Color::BLACK, Color::BLACK, Color::BLACK],
};

impl Brush {
    /// Returns a reference to the white brush.
    #[inline]
    pub fn white() -> &'static Self {
        &WHITE_BRUSH
    }

    /// Returns a reference to the black brush.
    #[inline]
    pub fn black() -> &'static Self {
        &BLACK_BRUSH
    }

    /// Returns a brush with a solid color.
    pub fn solid(color: &Color) -> Self {
        Self {
            colors: [*color, *color, *color, *color],
        }
    }

    /// Return this as shader data.
    pub(crate) fn to_shader_data(&self) -> [[f32; 4]; 4] {
        [
            self.colors[0].to_shader_data(),
            self.colors[1].to_shader_data(),
            self.colors[2].to_shader_data(),
            self.colors[3].to_shader_data(),
        ]
    }
}
