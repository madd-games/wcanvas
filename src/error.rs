use std::{
    error::Error,
    fmt::{self, Display},
};

/// Errors related to the WGPU backend.
#[derive(Debug)]
pub enum WgpuBackendError {
    AdapterNotFound,
}

impl Error for WgpuBackendError {}

impl Display for WgpuBackendError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            WgpuBackendError::AdapterNotFound => write!(f, "Adapter not found."),
        }
    }
}
