# wcanvas

This is a Rust crate which provides a 2D graphics library to be used with `winit`.

See the documentation for information on how to use the library.

**NOTE:** This crate is currently a work in progress.