use wcanvas::{
    brush::Brush,
    canvas::{Canvas, CanvasBackend},
    ctx::GraphicsContext,
    tex::Texture,
};
use winit::{
    dpi::LogicalSize,
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

struct State {
    demo_tex: Texture,
    red_brush: Brush,
    green_brush: Brush,
    blue_brush: Brush,
}

impl State {
    fn new(canvas: &mut Canvas<Window>) -> Self {
        let demo_png_data = include_bytes!("demo.png");
        let demo_image = image::load_from_memory(demo_png_data).unwrap();
        let bitmap = demo_image.into();

        Self {
            demo_tex: canvas.create_texture(&bitmap).unwrap(),
            red_brush: Brush::solid(&(255, 0, 0).into()),
            green_brush: Brush::solid(&(0, 255, 0).into()),
            blue_brush: Brush::solid(&(0, 0, 255).into()),
        }
    }

    fn draw(&self, context: &mut GraphicsContext) {
        context.set_texture(&self.demo_tex, &self.demo_tex.whole_rect());
        context.fill_rect(&(10, 10, 200, 200).into());

        context.set_brush(&self.red_brush);
        context.fill_rect(&(300, 10, 200, 200).into());

        context.set_brush(&self.green_brush);
        context.fill_rect(&(10, 300, 200, 200).into());

        context.set_brush(&self.blue_brush);
        context.fill_rect(&(300, 300, 200, 200).into());

        context.set_texture(&self.demo_tex, &(0, 0, 100, 100).into());
        context.set_brush(&self.red_brush);
        context.fill_rect(&(110, 110, 100, 100).into());

        context.set_texture(&self.demo_tex, &(100, 0, 100, 100).into());
        context.set_brush(&self.green_brush);
        context.fill_rect(&(300, 110, 100, 100).into());

        context.set_texture(&self.demo_tex, &(0, 100, 100, 100).into());
        context.set_brush(&self.blue_brush);
        context.fill_rect(&(110, 300, 100, 100).into());

        context.set_texture(&self.demo_tex, &(100, 100, 100, 100).into());
        context.set_brush(Brush::white());
        context.fill_rect(&(300, 300, 100, 100).into());
    }
}

#[tokio::main]
async fn main() {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("Image drawing demo")
        .with_inner_size(LogicalSize::new(600, 600))
        .build(&event_loop)
        .unwrap();
    let mut canvas = window.create_canvas().await.unwrap();
    let state = State::new(&mut canvas);

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Wait;

        match event {
            Event::RedrawRequested(window_id) if window_id == canvas.window().id() => {
                canvas
                    .draw_frame(|context| {
                        state.draw(context);
                    })
                    .unwrap();
            }
            Event::WindowEvent {
                ref event,
                window_id,
            } if window_id == canvas.window().id() => match event {
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                _ => {}
            },
            _ => {}
        }
    });
}
