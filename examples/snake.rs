use std::{
    collections::LinkedList,
    time::{Duration, Instant},
};

use rand::{rngs::ThreadRng, Rng};
use wcanvas::{
    brush::Brush,
    canvas::CanvasBackend,
    ctx::GraphicsContext,
    geo::{Dims, Pos},
};
use winit::{
    dpi::LogicalSize,
    event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

struct Snake {
    snake_cells: LinkedList<Pos>,
    cookie_cell: Pos,
    snake_brush: Brush,
    snake_head_brush: Brush,
    game_over_brush: Brush,
    cookie_brush: Brush,
    direction: Option<Pos>,
    next_update_time: Instant,
    rng: ThreadRng,
    is_game_over: bool,
}

const CELL_SIZE: u32 = 10;
const GRID_SIZE: u32 = 40;

impl Snake {
    fn new() -> Self {
        let mut snake_cells = LinkedList::new();

        for y in 20..25 {
            snake_cells.push_back(Pos::new(20, y));
        }

        Self {
            snake_cells,
            cookie_cell: Pos::new(3, 3),
            snake_brush: Brush::solid(&(20, 150, 20).into()),
            snake_head_brush: Brush::solid(&(50, 255, 50).into()),
            game_over_brush: Brush::solid(&(127, 20, 20).into()),
            cookie_brush: Brush::solid(&(150, 100, 20).into()),
            direction: None,
            next_update_time: Instant::now(),
            rng: rand::thread_rng(),
            is_game_over: false,
        }
    }

    fn update(&mut self) {
        if self.is_game_over {
            return;
        }

        let direction = match self.direction {
            Some(d) => d,
            None => return,
        };

        let mut next_head_pos = *self.snake_cells.front().unwrap() + direction;
        while next_head_pos.x < 0 {
            next_head_pos.x += GRID_SIZE as i32;
        }

        while next_head_pos.x >= GRID_SIZE as i32 {
            next_head_pos.x -= GRID_SIZE as i32;
        }

        while next_head_pos.y < 0 {
            next_head_pos.y += GRID_SIZE as i32;
        }

        while next_head_pos.y >= GRID_SIZE as i32 {
            next_head_pos.y -= GRID_SIZE as i32;
        }

        if self.snake_cells.contains(&next_head_pos) {
            self.is_game_over = true;
            return;
        }

        self.snake_cells.push_front(next_head_pos);

        if next_head_pos == self.cookie_cell {
            self.cookie_cell = Pos::new(
                self.rng.gen_range(0..GRID_SIZE as i32),
                self.rng.gen_range(0..GRID_SIZE as i32),
            );
        } else {
            self.snake_cells.pop_back();
        }
    }

    fn draw_cell(&self, context: &mut GraphicsContext, pos: &Pos) {
        context.fill_rect(&(*pos * CELL_SIZE as i32, Dims::new(CELL_SIZE, CELL_SIZE)).into());
    }

    fn draw(&self, context: &mut GraphicsContext) {
        context.set_brush(&self.cookie_brush);
        self.draw_cell(context, &self.cookie_cell);

        context.set_brush(&self.snake_head_brush);
        for cell in &self.snake_cells {
            self.draw_cell(context, cell);

            if self.is_game_over {
                context.set_brush(&self.game_over_brush);
            } else {
                context.set_brush(&self.snake_brush);
            }
        }
    }
}

#[tokio::main]
async fn main() {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("Snake on wcanvas")
        .with_inner_size(LogicalSize::new(
            CELL_SIZE * GRID_SIZE,
            CELL_SIZE * GRID_SIZE,
        ))
        .build(&event_loop)
        .unwrap();
    let mut canvas = window.create_canvas().await.unwrap();
    let mut snake = Snake::new();

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::WaitUntil(snake.next_update_time);

        match event {
            Event::RedrawRequested(window_id) if window_id == canvas.window().id() => {
                canvas
                    .draw_frame(|context| {
                        snake.draw(context);
                    })
                    .unwrap();
            }
            Event::MainEventsCleared => {
                if Instant::now() >= snake.next_update_time {
                    snake.next_update_time = Instant::now() + Duration::from_millis(125);
                    snake.update();
                    canvas.window().request_redraw();
                }
            }
            Event::WindowEvent {
                ref event,
                window_id,
            } if window_id == canvas.window().id() => match event {
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode,
                            ..
                        },
                    ..
                } => {
                    let current_dir = snake.direction.unwrap_or(Pos::new(0, 0));

                    match virtual_keycode {
                        Some(VirtualKeyCode::Escape) => *control_flow = ControlFlow::Exit,
                        Some(VirtualKeyCode::Up) => {
                            if current_dir.y != 1 {
                                snake.direction = Some(Pos::new(0, -1));
                            }
                        }
                        Some(VirtualKeyCode::Down) => {
                            if current_dir.y != -1 {
                                snake.direction = Some(Pos::new(0, 1));
                            }
                        }
                        Some(VirtualKeyCode::Left) => {
                            if current_dir.x != 1 {
                                snake.direction = Some(Pos::new(-1, 0));
                            }
                        }
                        Some(VirtualKeyCode::Right) => {
                            if current_dir.x != -1 {
                                snake.direction = Some(Pos::new(1, 0));
                            }
                        }
                        _ => {}
                    }
                }
                _ => {}
            },
            _ => {}
        }
    });
}
